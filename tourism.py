#! /usr/bin/env python3


class TourismAgency:

    drivers_list = []

    def __init__(self, code, name, address):
        self.code = code
        self.name = name
        self.address = address

    def show_info(self):

        return {'code': self.code, 'name': self.name, 'address': self.address}


class Driver:

    def __init__(self, id, code, name, last_name, sex, age, nationality, agency_code):
        self.id = id
        self.code = code
        self.name = name
        self.last_name = last_name
        self.sex = sex
        self.age = age
        self.nationality = nationality
        self.agency_code = agency_code

    def show_info(self):
        return {'id': self.id, 'name': self.name, 'last_name': self.last_name, 'sex': self.sex,
                'age': self.age, 'nationality': self.nationality}


class Tour:

    tourists = []

    def __init__(self, code, name, destination, date, cost, duration, driver_code):
        self.code = code
        self.name = name
        self.destination = destination
        self.date = date
        self.cost = cost
        self.duration = duration
        self.driver_code = driver_code


class Tourist:

    def __init__(self, id, name, last_name, nationality, sex, age):
        self.id = id
        self.name = name
        self.last_name = last_name
        self.nationality = nationality
        self.sex = sex
        self.age = age

    def show_info(self):
        return {'id': self.id, 'name': self.name, 'last_name': self.last_name, 'sex': self.sex,
                'age': self.age, 'nationality': self.nationality}


def main():

    print("Ingrese informacion de la agencia")
    code = input("codigo: ")
    name = input("nombre: ")
    address = input("direccion: ")
    agency = TourismAgency(code, name, address)
    drivers = int(input("Ingrese cantidad de conductores de la agencia: "))
    # agency.drivers_list = [Driver() for i in range(drivers)]
    print("Ingrese informacion de los conductores de la agencia")
    for i in range(drivers):
        print("Ingrese informacion del conductor ", i+1)
        id = int(input("Ingrese ID: "))
        code = int(input("Ingrese codigo: "))
        name = input("Ingrese nombre: ")
        last_name = input("ingrese apellido: ")
        sex = input("Ingrese sexo: ")
        age = int(input("Ingrese edad: "))
        nationality = input("Ingrese nacionalidad: ")
        agency_code = input("Ingrese codigo de agencia: ")
        agency.drivers_list.append(Driver(id, code, name, last_name, sex, age, nationality, agency_code))

    tours = []
    tour_cantity = int(input("Ingrese cantidad de tours"))
    for i in range(tour_cantity):
        print("Ingrese informacion del Tour")
        code = input("Ingrese codigo ")
        destiny = input("Ingrese destino ")
        date = input("Ingrese fecha ")
        cost = input("Ingrese costo ")
        duration = input("Ingrese duration ")
        driver_code = input("Ingrese codigo del conductor ")
        tours.append(Tour(code, destiny, destiny, date, cost, duration, driver_code))

    tourist_list = []
    tourists_cantity = int(input("Ingrese cantidad de turistas: "))
    for i in range(tourists_cantity):
        print("Ingrese informacion del turista ", i+1)
        id = int(input("ID: "))
        name = input("nombre: ")
        last_name = input("apellido: ")
        sex = input("sexo: ")
        age = int(input("edad: "))
        nationality = input("nacionalidad: ")
        tourist_list.append(Tourist(id, name, last_name, nationality, sex, age))

    print("Asociar turistas a los tours")
    for i in range(len(tours)):
        for j in range(len(tourist_list)):
            flag = int(input("¿El turista ", tourist_list[j].name, " pertenece al tour ", tours[i].name, " ? 1:si 0:no "))
            if flag == 1:
                tours[i].tourists.append(tourist_list[j])

    months = {'Enero': 0, 'Febrero': 0, 'Marzo': 0, 'Abril': 0, 'Mayo': 0, 'Junio': 0, 'Julio': 0,
                'Agosto': 0, 'Septiembre': 0, 'Octubre': 0, 'Noviembre': 0, 'Diciembre': 0}

    for i in range(len(tours)):
        for j in range(len(months)):
            if tours[i].date == 'Enero':
                months[j] += 1

    print("total conductores = ", len(agency.drivers.list))
    print("Total turistas = ", len(tourist_list))
    # ------------------------------------------------------
    # Turistas por género

    male_tourist = []
    female_tourist = []
    for i in range(len(tourist_list)):
        if tourist_list[i].sex == 'm':
            male_tourist.append(tourist_list[i])
        elif tourist_list[i].sex == 'f':
            female_tourist.append(tourist_list[i])
    # ------------------------------------------------------
    # Turistas por nacionalidad
    countries = []
    for i in range(len(tourist_list)):
        if tourist_list[i].nationality not in countries:
            countries.append({tourist_list[i].nationality: []})
    for i in range(len(tourist_list)):

        for countrie in countries:
            for key, value in countrie.items():
                if tourist_list[i].nationality == key:
                    value.append(tourist_list[i])
    for countrie in countries:
        for key, value in countrie.items():
            print(key, value)
    # ------------------------------------------------------



if __name__ == '__main__':
    main()
