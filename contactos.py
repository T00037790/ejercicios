
flag = 0
dictionary = {}
print("Ingrese la informacion de los contactos")


while flag == 0:
    aux = 0
    corr = 0
# --------------------------------------------------------------------------------------------------------
    try:
        name = input("Ingrese nombre del contacto: ")
    except (ValueError):
        print("Error digitando los datos, intente nuevamente")
        continue

    if name in dictionary:
        print("El nombre ya se encuentra en la lista de contactos")
        #aux = 1
        continue
# --------------------------------------------------------------------------------------------------------
    while corr == 0:

        try:
            phone = int(input("Ingrese telefono del contacto: "))
            if phone in dictionary.values():
                print("El numero de telefono ya se encuentra en la lista de contactos")
                aux = 1
                corr = 0
            else:
                corr = 1


        except (NameError, ValueError, TypeError):
            print("Error!, ingrese un valor numerico")


    if aux == 0:
        dictionary.setdefault(name, phone)

    flag = int(input("Desea ingresar otro usuario? 0: SI, 1: NO "))


# ---------------------------------------------------------------------------------


print("Lista de contactos: ")
for key in dictionary:
    print(key, dictionary[key])

print("Hay un total de ", len(dictionary), "contactos en la lista")