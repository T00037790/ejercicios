#! /usr/bin/env python3
import http.server
import socketserver
from http.server import HTTPServer, BaseHTTPRequestHandler
from os import curdir, sep
from io import BytesIO

PORT = 8080
Handler = http.server.SimpleHTTPRequestHandler


def some_function():
    pass


class myHandler(BaseHTTPRequestHandler):

    def _set_headers(self, type):
        self.send_response(200)
        self.send_header('Content-type', type)
        self.end_headers()

    # Handler for the GET requests
    def do_GET(self):
        if self.path == '/home' or self.path == '/':
            self.path = "templates/home.html"
            f = open(curdir + sep + self.path, 'r')
            self._set_headers('text/html')
            self.wfile.write(bytes(f.read(), "utf-8"))
            f.close()
            return
        elif self.path == '/about':
            self.path = "templates/about.html"
            f = open(curdir + sep + self.path, 'r')
            self._set_headers('text/html')
            self.wfile.write(bytes(f.read(), "utf-8"))
            f.close()
        elif self.path == '/coffee.jpeg':
            i = open(curdir + sep + 'img/coffee.jpeg', 'rb')
            self._set_headers('image/jpeg')
            self.wfile.write(bytes(i.read()))
            i.close()
            return
        elif self.path == '/contact':
            self.path = "templates/contact.html"
            f = open(curdir + sep + self.path, 'r')
            self._set_headers('text/html')
            self.wfile.write(bytes(f.read(), "utf-8"))
            f.close()
        elif self.path == '/style.css':
            f2 = open(curdir + sep + 'templates/css/style.css', 'r')
            self._set_headers('text/css')
            self.wfile.write(bytes(f2.read(), "utf-8"))
            f2.close()
        elif self.path == '/message.js':
            f3 = open(curdir + sep + 'templates/js/message.js', 'r')
            self._set_headers('text/javascript')
            self.wfile.write(bytes(f3.read(), "utf-8"))
            f3.close()
            return
        else:
            self.send_response(404)
            self.path = "templates/error.html"
            f = open(curdir + sep + self.path, 'r')
            self._set_headers('text/html')
            self.wfile.write(bytes(f.read(), "utf-8"))
            f.close()

        return

    def do_POST(self):
        print("Incoming URL: ", self.path)
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)  # <--- Gets the data itself
        self.send_response(200)
        self.end_headers()
        strvalue = body.decode('utf-8')
        a = strvalue.split('&')
        dictionary = dict(s.split('=') for s in a)
        print(dictionary)
        response = BytesIO()
        for k, v in dictionary.items():
            line = str(k) + " = " + str(v) + '\n'
            response.write(bytes(line, "utf-8"))
        self.wfile.write(response.getvalue())


try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT), myHandler)
    print('Started httpserver on port ', PORT)
    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print('^C received, shutting down the web server')
    server.socket.close()
