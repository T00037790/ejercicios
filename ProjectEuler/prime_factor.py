
primos = []

num = 600851475143

mitad = int(num/2)+1

for j in range(2, mitad):
  list = []
  for i in range(1, j+1):
    if (j%i == 0):
      list.append(i)
  if len(list)==2:
    primos.append(j)

print("primos = ", primos[-1:])

prime_factors = []
for prime in primos:
    if num % prime == 0:
        prime_factors.append(prime)

print(prime_factors[-1:])