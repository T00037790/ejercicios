#!/usr/bin/env python3

def primos(numero):
	aux = 0
	for i in range(1, numero+1):
		if numero % i == 0:
			aux = aux + 1
			if aux > 2:
				break

	if aux <=2:
        	return numero
	else:
        	return 0
n = 1
total = 0
while n <= 2000000:
	n = n + 1
	r = primos(n)
	total = total + r

print(total)

