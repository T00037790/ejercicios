#!/usr/bin/env python3


def first_condition(a, b, c):
	if a < b and b < c and a < c:
		return True
	else:
		return False

def second_condition(a, b, c):
	if (a*a) + (b*b) == (c*c):
		return True
	else:
		return False

def third_condition(a, b, c):
	if a + b + c == 1000:
		return True
	else:
		return False

flag = 0
for i in range(1000):
	for j in range(1000):
		for k in range(1000):
			if first_condition(i, j, k):
				#print(i, j, k)
				if second_condition(i, j, k):
					if third_condition(i, j, k):
						print("Special Pythagorean triplet is: ", i, j, k)
						print("Producto = ", i*j*k)
						flag = 1
						break
			if flag == 1:
				break
	if flag == 1:
		break
















