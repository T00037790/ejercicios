
# tabla de posiciones

while True:
    try:
        teams = int(input("Ingrese cantidad de equipos: "))
        break
    except:
        print("Ingrese un valor numerico")

print("Ingrese información de los partidos: ")

while True:
    try:
        played = int(input("partidos jugados"))
        break
    except:
        print("Ingrese un valor numerico")

team_names = []
goals = []
total_points = []
matches = []
results = []

for i in range(1, teams+1):
    print("Para el equipo", i, " ingrese la siguiente informacion")
    team = input("Ingrese Nombre del equipo: ")
    team_names.append(team)
    PG = 0
    PE = 0
    PP = 0
    while PG+PE+PP != played:
        PG = int(input("partidos ganados"))
        PE = int(input("partidos empatados"))
        PP = int(input("partidos perdidos"))
        if PG + PE + PP != played:
            print("La cantidad de partidos no coincide con los resultados de los partidos, por favor ingrese nuevamente")

    result = [PG, PE, PP]
    results.append(result)

    GF = int(input("Ingrese cantidad de goles a favor: "))
    GC = int(input("Ingrese cantidad de goles en contra: "))
    DF = GF - GC
    goals.append(DF)

    match = [GF, GC]
    matches.append(match)

    points = PG*3 + PE*1
    total_points.append(points)

def sort(A, B, C, D, E):
    for i in range(1,len(A)):
        for j in range(0,len(A)-i):
            if A[j+1] > A[j]:
                aux=A[j]
                A[j]=A[j+1]
                A[j+1]=aux

                aux = B[j]
                B[j] = B[j + 1]
                B[j + 1] = aux

                aux = C[j]
                C[j] = C[j + 1]
                C[j + 1] = aux

                aux = D[j]
                D[j] = D[j + 1]
                D[j + 1] = aux

                aux = E[j]
                E[j] = E[j + 1]
                E[j + 1] = aux


            if A[j + 1] == A[j]:
                if C[j + 1] > C[j]:
                    aux = A[j]
                    A[j] = A[j + 1]
                    A[j + 1] = aux

                    aux = B[j]
                    B[j] = B[j + 1]
                    B[j + 1] = aux

                    aux = C[j]
                    C[j] = C[j + 1]
                    C[j + 1] = aux

                    aux = D[j]
                    D[j] = D[j + 1]
                    D[j + 1] = aux

                    aux = E[j]
                    E[j] = E[j + 1]
                    E[j + 1] = aux

sort(total_points, team_names, goals, matches, results)

print("Tabla de Posiciones ")

for i in range(teams):
    print(team_names[i], " ", "PT: ", total_points[i], "PG: ", results[i][0], "PE: ", results[i][1],"PP: ", results[i][2], "DG: ", goals[i], "GF: ", matches[i][0], "GC: ", matches[i][1], )
