

f = open("grid.txt", "r")
matrix = []
cont = 0
for lines in f:
    aux = lines.split()
    results = [int(i) for i in aux]
    matrix.append(results)
    cont += 1
f.close()

for i in range(cont):
    print(matrix[i])

def left_right(matrix):
    mayor = 0
    for i in range(20):
        for j in range(17):
            slice = matrix[i][j:4 + j]
            prod = 1
            for item in slice:
                prod = int(item) * prod
            if prod > mayor:
                mayor = prod
                largest = slice
    print("The four numbers with the largest product in left-right direction is: ", largest)

    return mayor


def up_down(matrix):
    mayor = 0
    for i in range(len(matrix[0])-1):
        for j in range(len(matrix[0])-4):
            slice = [x[i] for x in matrix[j:j+4]]
            prod = 1
            for item in slice:
                prod = int(item) * prod
            if prod > mayor:
                mayor = prod
                largest = slice
    print("The four numbers with the largest product in up-down direction is: ", largest)

    return mayor


def diagonally(matrix):
    #for i in range(len(matrix[0])):
    #   for j in range(len(matrix[0])):
    #        print(matrix[i][j])
    l = len(matrix[0])
    print([matrix[i][i] for i in range(l)])
    print([matrix[l-1-i][i] for i in range(l-1, -1, -1)])
    diags = [matrix[l-1-i][i] for i in range(l-1, -1, -1)]
    return diags

print(left_right(matrix))
print(up_down(matrix))
print(diagonally(matrix))


"""
largest = [up_down(matrix), left_right(matrix), diagonally(matrix)]

print("el mayor valor es: ", max(largest))


for i in range(20):
    for j in range(17):
        slice = matrix[i][j:4+j]
        print(slice)
"""

