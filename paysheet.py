#!/usr/bin/env python3

print("Por favor ingrese información referente a los trabajadores de la empresa")

flag = 0
transport_assistance = 72250
monthly_salary = 850000
workers = []
total_to_pay = 0

while flag == 0:
    dict = {}
    name = input("Ingrese nombre del trabajador: ")
    worked_days = int(input("Ingrese total de dias laborados: "))
    salary_by_worker = int(input("Ingrese Salario Mensual: "))
    ord_hour = salary_by_worker*monthly_salary / 240
    day_overtime = int(input("Ingrese cantidad de Horas Extras Diurnas: "))
    nightly_hours = int(input("Ingrese cantidad de Horas Nocturnas: "))
    nightly_hours_overtime = int(input("Ingrese cantidad de Horas Extras Nocturnas: "))
    sun_and_holy_hours = int(input("Ingrese cantidad de Horas Extras Domigos y Festivos: "))
    nightly_sun_and_holy_hours = int(input("Ingrese cantidad de Horas Extras Nocturnas Domigos y Festivos: "))

    # --------------------------------------------------------------------------
    if salary_by_worker <= 2:
        salary = (salary_by_worker * monthly_salary) + transport_assistance
    else:
        salary = salary_by_worker * monthly_salary
    layoffs = (salary_by_worker*worked_days)/360
    interest_on_layoffs = (layoffs*worked_days*0.12)/360
    service_bonus = (salary*180)/360
    holidays = (monthly_salary*worked_days)/720
    settlement = layoffs + interest_on_layoffs + service_bonus + holidays
    net_paid = settlement + salary
    dict = {'Nombre': name, 'HT': worked_days, 'SM': [salary_by_worker, salary_by_worker * monthly_salary],
            'HED': [day_overtime, round(ord_hour * 1.25, 2)], 'HN': [nightly_hours, round(ord_hour * 1.35, 2)],
            'HEN': [nightly_hours_overtime, round(ord_hour * 1.75, 2)],
            'HEDF': [sun_and_holy_hours, round(ord_hour * 2, 2)], 'HENDF': [nightly_sun_and_holy_hours, round(ord_hour * 2.5, 2)],
            'Liquidacion': {'Cesantias': layoffs, 'Interés sobre cesantias': round(interest_on_layoffs, 2),
                            'Vacaciones': holidays, 'Primas': service_bonus}}

    total_to_pay += net_paid
    # ---------------------------------------------------------------------------

    workers.append(dict)
    print("¿Desea agregar otro trabajador?")
    flag = int(input("Si --> Ingrese 0  | No --> Ingrese 1: <<>> "))

print("Información de los trabajadores")
for worker in workers:
    print(worker)

print("La empresa pagó un total de: $ {}".format(round(total_to_pay, 2)))

