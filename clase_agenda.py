#!/usr/bin/env python3


class Agenda:

    def __init__(self):
        self.phone_book = []

    def save_contacts(self, id, name, phone_number, address, **kwargs):
        whatsapp = kwargs.setdefault('whatsapp')
        telegram = kwargs.setdefault('telegram')
        self.phone_book.append({'id': id, 'name': name, 'phone': phone_number, 'address': address,
                                'whatsapp': whatsapp, 'telegram': telegram})

    def list_contacts(self):
        print("--" * 10)
        for i in range(len(self.phone_book)):
            print("Contacto ", i + 1)
            for key, value in self.phone_book[i].items():
                print(key, value)
            print("--" * 10)


    def edit_contact(self):
        f = int(input("¿Desea editar contacto? 1: si  0:no "))
        if f == 1:
            pos = int(input("Ingrese id del contacto: "))
            for i in range(len(self.phone_book)):
                for key, value in self.phone_book[i].items():
                    if pos == value:
                        name = input("Digit new name: ")
                        phone_number = int(input("Digit phone number: "))
                        self.phone_book[i].update({'name': name, 'phone': phone_number})

    def delete_contact(self):
        f = int(input("¿Desea eliminar contacto ? 1: si  0:no "))
        if f == 1:
            pos = int(input("Ingrese id del contacto: "))
            for i in range(len(self.phone_book)):
                for key, value in self.phone_book[i].items():
                    if pos == value:
                        del self.phone_book[i]


class Contact:

    id = 0
    name = ''
    phone_number = ''
    address = ''
    whatsapp = ''
    telegram = ''


def main():
    stopper = 1
    agenda_google = Agenda()

    while stopper:
        phone_number = []
        whatsapp = 0
        telegram = 0
        name = input("Digit  contact name: ")
        id = int(input("Digit contact id: "))
        address = input("Digit address: ")

        flag = 1
        while flag:
            phone_number.append(int(input("Digit phone number: ")))
            flag = int(input("¿Desea ingresar otro telefono? 1: si, 0: no "))

        flag2 = 1
        while flag2:
            flag2 = int(input("¿Desea ingresar whatsapp? 1: si, 0: no "))
            if flag2 == 1:
                whatsapp = int(input("Ingrese Whatsapp: "))
            flag2 = 0

        flag3 = 1
        while flag3:
            flag3 = int(input("¿Desea ingresar telegram? 1: si, 0: no "))
            if flag3 == 1:
                telegram = input("Ingrese telegram: ")
            flag3 = 0

        stopper = int(input("¿Desea ingresar otro contacto?  1: si, 0: no"))
        agenda_google.save_contacts(id, name, phone_number, address, whatsapp=whatsapp, telegram=telegram)
        agenda_google.list_contacts()
        agenda_google.edit_contact()
        agenda_google.list_contacts()
        agenda_google.delete_contact()
        agenda_google.list_contacts()


if __name__ == "__main__":
    main()
