#! /usr/bin/env python3
import numpy as np
import sys
import pyximport

sys.setrecursionlimit(999999999)

easy = np.matrix([[8, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 3, 6, 0, 0, 0, 0, 0],
                  [0, 7, 0, 0, 9, 0, 2, 0, 0],
                  [0, 5, 0, 0, 0, 7, 0, 0, 0],
                  [0, 0, 0, 0, 4, 5, 7, 0, 0],
                  [0, 0, 0, 1, 0, 0, 0, 3, 0],
                  [0, 0, 1, 0, 0, 0, 0, 6, 8],
                  [0, 0, 8, 5, 0, 0, 0, 1, 0],
                  [0, 9, 0, 0, 0, 0, 9, 0, 0]])
"""
easy = np.matrix([[0, 0, 0, 0, 3, 6, 0, 7, 0],
                  [3, 6, 0, 0, 1, 0, 9, 5, 0],
                  [0, 8, 0, 5, 9, 0, 0, 0, 0],
                  [8, 0, 0, 0, 0, 0, 6, 0, 0],
                  [2, 4, 1, 0, 0, 0, 5, 3, 7],
                  [0, 0, 3, 0, 0, 0, 0, 0, 9],
                  [0, 0, 0, 0, 8, 5, 0, 6, 0],
                  [0, 5, 8, 0, 7, 0, 0, 9, 3],
                  [0, 1, 0, 2, 4, 0, 0, 0, 0]])


"""

def get_empty():
    empties = []
    for i in range(9):
        for j in range(9):
            if easy[i, j] == 0:
                empties.append((i, j))
    print(empties)
    print(len(empties))
    tupla = empties[0]
    i = tupla[0]
    j = tupla[1]
    print(i, j, "tuplas")
    return empties


empties = get_empty()


def north(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[:3, 3:6]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def south(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[6:9, 3:6]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def east(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[3:6, 6:9]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def west(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[3:6, :3]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def center(i, j, *tail):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[3:6, 3:6]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def north_east(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[:3, 6:9]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def north_west(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[:3, :3]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def south_east(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[6:9, 6:9]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def south_west(i, j):
    global cont
    cont = 0
    for candidate in range(easy[i, j], 10):
        if candidate not in easy[6:9, :3]:
            if candidate not in easy[i]:
                if candidate not in easy[:, j]:
                    easy[i, j] = candidate
                    cont = 1
                    return skip(i, j)
                    break
    skip_back(i, j) if cont == 0 else none


def skip(i, j):
    cont = 0
    for tupla in empties:
        cont += 1
        if i == tupla[0] and j == tupla[1]:
            temp = empties[cont]
            i = temp[0]
            j = temp[1]
            step(i, j)
            break


def skip_back(i, j):
    cont = 0
    for tupla in empties:
        cont += 1
        if i == tupla[0] and j == tupla[1]:
            easy[i, j] = 0
            if i == 0 and j == 0:
                easy[i, j] = easy[i, j] + 1
                tail = easy[i, j]
                step(i, j, tail)
                break
            temp = empties[cont - 2]
            i = temp[0]
            j = temp[1]
            easy[i, j] = easy[i, j] + 1
            tail = easy[i, j]
            step(i, j, tail)
            break


def step(i, j, *args):
    if fin(i, j):
        print(easy)
    else:
        # north
        if 0 <= i < 3 and 2 < j < 6:
            north(i, j)

        # south
        if 5 < i < 9 and 2 < j < 6:
            south(i, j)

        # east
        if 2 < i < 6 and 5 < j < 9:
            east(i, j)

        # west
        if 2 < i < 6 and 0 <= j < 3:
            west(i, j)

        # center
        if 2 < i < 6 and 2 < j < 6:
            center(i, j)

        # north east
        if 0 <= i < 3 and 5 < j < 9:
            north_east(i, j)

        # north west
        if 0 <= i < 3 and 0 <= j < 3:
            north_west(i, j)

        # south east
        if 5 < i < 9 and 5 < j < 9:
            south_east(i, j)

        # south west
        if 5 < i < 9 and 0 <= j < 3:
            south_west(i, j)


def fin(i, j):
    last = empties[46]
    if i == last[0] and j == last[1]:
        return True
    else:
        return False


tupla = empties[0]
step(tupla[0], tupla[1])
