#! /usr/bin/env python3
import requests
import glob
from dominate import document
from dominate.tags import *
import dominate

base_url = 'https://jsonplaceholder.typicode.com'


def get_json(uri):
    url = base_url + uri
    response = requests.get(url)
    response_json = response.json()
    return response_json


users = get_json('/users')
posts = get_json('/posts')
comments = get_json('/comments')
todos = get_json('/todos')
photos = get_json('/photos')


print(posts)
doc = dominate.document(title='LSV TEAM')

with doc.head:
    link(href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
         rel="stylesheet", integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T",
         crossorigin="anonymous")
    link(rel="stylesheet", href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
         integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm",
         crossorigin="anonymous")
    script(src="https://code.jquery.com/jquery-3.2.1.slim.min.js",
           integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN",
           crossorigin="anonymous")

    script(src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js",
           integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q",
           crossorigin="anonymous")

    script(src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
           integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl",
           crossorigin="anonymous")


with doc.body:
    with div(_class='container'):
        h2(strong('PHOTOS'))
        cont = 1
        with div(_class='container-fluid'):
            with div(_class ='row flex-row flex-nowrap'):
                with div(_class='row'):
                        for photo in photos:
                            for key, value in photo.items():
                                if key == 'thumbnailUrl':
                                    if cont < 13:
                                        with div(_class='col-2'):
                                                div(img(src=value, align="center"), _class='card card-block', style="width: 150px")
                                                cont += 1
        br()
        h2(strong('USERS'))
        cont = 0
        with div(_class='table-responsive'):
            with table(_class='table table-hover'):
                    for user in users:
                        with thead(_class='thead-dark'):
                            with tr():
                                if cont == 0:
                                    #with thead(_class='thead-dark'):
                                        for key, value in user.items():
                                            with td(_align='center'):
                                                strong(key.upper())
                                            cont += 1
                                else:
                                    for key, value in user.items():
                                        if key == 'address':
                                            for k, v in value.items():
                                                if k == 'street':
                                                    td(v)
                                        elif key == 'company':
                                            for k, v in value.items():
                                                if k == 'name':
                                                    td(v)
                                        else:
                                            td(value)
        br()
        h2(strong('TODO\'s'))
        cont = 0
        with div(_class='table-responsive-sm'):
            with table(_class='table table-striped'):
                for todo in todos:
                    if cont == 0:
                        with thead(_class='thead-dark'):
                            with tr(_align='center'):
                                for key, value in todo.items():
                                    with th(_scope='col'):
                                        strong(key.upper())
                                cont += 1
                    else:
                        with tbody():
                            with tr():
                                if cont < 11:
                                    for key, value in todo.items():
                                        with th(_scope='col'):
                                            strong(value)
                                    cont += 1
        br()
        h2(strong('COMMENTS'))
        cont = 0
        with div(_class='table-responsive-sm', _align='center'):
                for comment in comments:
                    if cont < 10:
                        with div(_class='card text-center', _style="width: 500px;"):
                            for key, value in comment.items():

                                    if key == 'name':
                                        name = value

                                    if key == 'body':
                                        body = value

                                    if key == 'email':
                                        email = value

                            with div(_class='card-header'):
                                p(name)
                            with div(_class='card-body'):
                                p(body)
                            with div(_class='card-footer text-muted'):
                                p(email)
                            cont += 1
                        br()


with open('index2.html', 'w') as f:
    f.write(doc.render())
